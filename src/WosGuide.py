#    This file is part of wos-manager.
#
#   wos-manager is free software: you can redistribute it and/or modify it under the terms of the GNU Affero Public License
#   as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#   wos-manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU Affero Public License for more details.
#
#   You should have received a copy of the GNU Affero Public License along with wos-manager. If not, see <https://www.gnu.org/licenses/>. 

import sys
import re
import math
import sqlite3
import operator
import filehelper as FileHelper
from PySide6.QtWidgets import QApplication, QDialog, QLineEdit, QPushButton, QVBoxLayout, QLabel, QWidget, QScrollArea, QHBoxLayout, QGridLayout, QSizePolicy, QCheckBox, QColorDialog, QComboBox
from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon, QColor, QPalette, QFont, QPixmap, QIntValidator, QDoubleValidator
from enum import Enum
from pathlib import Path

RecordData = FileHelper.Config["RecordData"]
Table = FileHelper.Config["Table"]
MaxResults = FileHelper.Config["MaxResults"]
IsUserStupid = FileHelper.Config["AllowSemitransparentPlanetIcons"]

DatabaseCursor = sqlite3.connect(FileHelper.UniverseMapPath)

#a enum containing all datatypes that are supported by QMiscDataInpitWidget
class DataType(Enum):
    BOOL     = 1
    ENUM     = 2
    STRING   = 3
    INT      = 4
    FLOAT    = 5
    FAKEBOOL = 6 #since resorces like iron arent really bool values in the database, i give them a special type

DataTypeOperators = {
        DataType.BOOL    : [],
        DataType.ENUM    : ["IN", "NOT IN"],
        DataType.STRING  : ["=", "!=", "LIKE", "NOT LIKE"],
        DataType.INT     : ["=", ">", ">=", "<", "<=",],
        DataType.FLOAT   : ["=", ">", ">=", "<", "<=",],
        DataType.FAKEBOOL: []
        }

#a function to turn RGB ito HSV
#i use this function for the sole reason that the default hsv getter returns with a very low precision
def RGBtoHSV(R,G,B): 
    M = max(R,G,B)
    m = min(R,G,B)

    if R == G and G == B: #if all valus are the same then set the hue to 0 so it dosent try to divide by 0
        H = 0
    else:
        H = math.degrees(math.acos((R-G/2-B/2)/(R**2+G**2+B**2-R*G-R*B-G*B)**0.5))
        H = 360 - H if B > G else H
    S = 1-m/M if M > 0  else 0
    V = M/255

    return(H,S,V)

def RankQuery(Query, SelectedMainColor=None, SelectedSecondaryColor=None):
    BestMatches = []
    for Line in Query: # finding top list of closest planet colors
        # loading data
        DeltaColor = 0
        Cords = Line[0]
        MainColor = RGBtoHSV(*Line[1:4])
        SecondaryColor = RGBtoHSV(*Line[4:7])

        if SelectedMainColor:
            # hue difference
            DeltaColor += abs(SelectedMainColor[0] - MainColor[0]) / 360
            #saturation difference
            DeltaColor += abs(SelectedMainColor[1] - MainColor[1])  

        if SelectedSecondaryColor:
            # hue difference
            DeltaColor += abs(SelectedSecondaryColor[0] - SecondaryColor[0]) / 360
            #saturation difference
            DeltaColor += abs(SelectedSecondaryColor[1] - SecondaryColor[1])
        BestMatches.append((Cords, DeltaColor))
    list.sort(BestMatches, key=operator.itemgetter(1))
    return BestMatches

#{{{ QPlanetInfoDialog: dialog that displays simple info about the given planet
class QPlanetInfoDialog(QDialog):
    def __init__(self, Coords):
        super().__init__()
        self.Layout = QGridLayout(self)

        #i make this query super long so that i ensure the records are ALWAYS in the same order
        Query = DatabaseCursor.execute("select {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} from {} where {} like \"{}\"".format(
            RecordData["Name"]["Name"],
            RecordData["RingType"]["Name"],
            RecordData["PlanetType"]["Name"],
            RecordData["Temperature"]["Name"],
            RecordData["DayCycleIncrement"]["Name"],
            RecordData["Atmosphere"]["Name"],
            RecordData["Gravity"]["Name"],
            RecordData["RandomMat"]["Name"],
            RecordData["Diamond"]["Name"],
            RecordData["Copper"]["Name"],
            RecordData["Iron"]["Name"],
            RecordData["Beryllium"]["Name"],
            RecordData["Coal"]["Name"],
            RecordData["Lead"]["Name"],
            RecordData["Aluminum"]["Name"],
            RecordData["Uranium"]["Name"],
            RecordData["Gold"]["Name"],
            RecordData["Jade"]["Name"],
            RecordData["Titanium"]["Name"],
            RecordData["PrimaryRed"]["Name"],
            RecordData["PrimaryGreen"]["Name"],
            RecordData["PrimaryBlue"]["Name"],
            RecordData["SecondaryRed"]["Name"],
            RecordData["SecondaryGreen"]["Name"],
            RecordData["SecondaryBlue"]["Name"],
            Table,
            RecordData["Cords"]["Name"],
            Coords))
        PlanetInfo = Query.fetchall()[0]
        BigFont = QFont()
        BigFont.setPointSize(16)


        self.LabelContainer = QWidget()
        self.LabelContainerLayout = QVBoxLayout(self.LabelContainer)
       
        self.NameLabel = QLabel(str(PlanetInfo[0]))
        self.NameLabel.setFont(BigFont)

        #getting the correct planet image
        if PlanetInfo[1] == "None":
            PlanetImage = QPixmap(FileHelper.RingedPlanetIconPath)
        else:
            PlanetImage = QPixmap(FileHelper.PlanetIconPath)

        if not IsUserStupid:
            PlanetMask = PlanetImage.createMaskFromColor(QColor("transparent"), Qt.MaskInColor)
            PlanetImage.fill(QColor(PlanetInfo[19], PlanetInfo[20], PlanetInfo[21]))
            PlanetImage.setMask(PlanetMask)
        else:
            PlanetTransformImage = PlanetImage.toImage()
            for x in range(PlanetTransformImage.width()):
                for y in range(PlanetTransformImage.height()):
                    Color = PlanetTransformImage.pixelColor(x, y)
                    PlanetTransformImage.setPixelColor(x, y, QColor(PlanetInfo[19], PlanetInfo[20], PlanetInfo[21], Color.alpha()))

            PlanetImage = PlanetImage.fromImage(PlanetTransformImage)


        self.TypeLabel = QLabel("Planet Type: {}".format(str(PlanetInfo[2])))

        self.TemperatureLabel = QLabel("Temperature: {}".format(str(PlanetInfo[3])))
        if PlanetInfo[3] >= 120:
            self.TemperatureLabel.setStyleSheet("color: red")
        if PlanetInfo[3] <= 20:
            self.TemperatureLabel.setStyleSheet("color: blue")

        self.TidalLockLabel = QLabel("Tidally Locked: {}".format(str(PlanetInfo[4] == 0)))
        self.AtmosphereLabel = QLabel("Atmosphere: {}".format(str(PlanetInfo[5] == 1)))
        self.GravityLabel = QLabel("Gravity: {}".format(str(PlanetInfo[6])))

        #kinda a mess but it has to be this way, so :shrug:
        self.ResourceLabel = QLabel()
        ResourceList = "Resources: "
        
        for i, resource in enumerate(["Diamond", "Copper", "Iron", "Beryllium", "Coal", "Lead", "Aluminum", "Uranium", "Gold",  "Jade", "Titanium"]):
            if PlanetInfo[i + 8] > 0:
                ResourceList += " {}, ".format(resource)

        self.ResourceLabel.setText(ResourceList)

        self.RandomMatLabel = QLabel(str("Random Material: {}".format(PlanetInfo[7])))
        self.RingTypeLabel = QLabel(str("Ring Type: {}".format(PlanetInfo[1])))

        self.LabelContainerLayout.addWidget(self.TypeLabel)
        self.LabelContainerLayout.addWidget(self.TemperatureLabel)
        self.LabelContainerLayout.addWidget(self.TidalLockLabel)
        self.LabelContainerLayout.addWidget(self.AtmosphereLabel)
        self.LabelContainerLayout.addWidget(self.GravityLabel)
        self.LabelContainerLayout.addWidget(self.RingTypeLabel)
        self.LabelContainerLayout.addWidget(self.ResourceLabel)
        self.LabelContainerLayout.addWidget(self.RandomMatLabel)

        self.MainColorWidget = QWidget()
        self.MainColorWidget.setStyleSheet('background-color: {}'.format(QColor(PlanetInfo[19], PlanetInfo[20], PlanetInfo[21]).name(QColor.HexRgb)))
        self.MainColorWidget.setMinimumSize(150, 75)

        self.SecondaryColorWidet = QWidget()
        self.SecondaryColorWidet .setStyleSheet('background-color: {}'.format(QColor(PlanetInfo[22], PlanetInfo[23], PlanetInfo[24]).name(QColor.HexRgb)))
        self.SecondaryColorWidet .setMinimumSize(150, 75)

        self.PlanetIconWidget = QLabel()
        self.PlanetIconWidget.setPixmap(PlanetImage)

        self.OkButton = QPushButton("ok")
        self.OkButton.clicked.connect(self.reject)


        self.Layout.addWidget(self.NameLabel, 0, 0, 1, 3)
        self.Layout.addWidget(self.PlanetIconWidget, 1, 0)
        self.Layout.addWidget(self.LabelContainer, 1, 1)
        self.Layout.addWidget(QLabel("Main Color"), 2, 1)
        self.Layout.addWidget(QLabel("Secondary Color"), 2, 3)
        self.Layout.addWidget(self.MainColorWidget, 3, 1)
        self.Layout.addWidget(self.SecondaryColorWidet, 3, 3)
        self.Layout.addWidget(self.OkButton, 4, 3)
#}}}

#{{{ QColorInputWidget: a widget used to input colors
class QColorInputWidget(QPushButton):
    def __init__(self):
        super().__init__()
        self.setMinimumSize(100, 100)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)

        self.Result = QColor(0, 255, 0)

        self.ColorPicker = QColorDialog()

        self.clicked.connect(self.PickColor)
        self.setFlat(True)
        self.setAutoFillBackground(True)
        self.setAttribute(Qt.WA_StyledBackground, True)
        self.setStyleSheet('background-color: {}'.format(self.Result.name(QColor.HexRgb)))

    def PickColor(self):
        SelectedColor = self.ColorPicker.getColor(self.Result)
        if SelectedColor.isValid():
            self.RegisterChange(SelectedColor)

    def RegisterChange(self, NewValue):
        self.Result = NewValue
        self.setStyleSheet('background-color: {}'.format(self.Result.name(QColor.HexRgb)))

    def GetResults(self):
        return RGBtoHSV(self.Result.red(), self.Result.green(), self.Result.blue())
#}}}

#{{{ QColorInputContainer: widget that contains input widgets for the color and labels
class QColorInputContainer(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QGridLayout(self)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        self.setMaximumHeight(250)

        self.MainColorCheckbox = QCheckBox("Main Color")

        self.MainColorInput = QColorInputWidget()

        self.SecondaryColorCheckbox = QCheckBox("Secondary Color")

        self.SecondaryColorInput = QColorInputWidget()

        self.Layout.addWidget(self.MainColorCheckbox, 0, 0)
        self.Layout.addWidget(self.MainColorInput, 1, 0)
        self.Layout.addWidget(self.SecondaryColorCheckbox, 0, 1)
        self.Layout.addWidget(self.SecondaryColorInput, 1, 1)

    def IsEnabled(self):
        return self.MainColorCheckbox.isChecked() or self.SecondaryColorCheckbox.isChecked()

    def GetMainColor(self):
        if self.MainColorCheckbox.isChecked():
            return self.MainColorInput.GetResults()
        else:
            return None

    def GetSecondaryColor(self):
        if self.SecondaryColorCheckbox.isChecked():
            return self.SecondaryColorInput.GetResults()
        else:
            return None
#}}}

#{{{ QLineInputWidget: widget that shows a list of bool values and lets the user set them accordingly, used for the ENUM data type
class QListInputWidget(QWidget):
    def __init__(self, PossibleValues):
        super().__init__()
        self.Layout = QVBoxLayout(self)

        CheckBoxes = []

        for i in PossibleValues:
            CheckBox = QCheckBox(i)
            CheckBox.setFixedSize(100, 20)
            self.Layout.addWidget(CheckBox, Qt.AlignRight)
            CheckBoxes.append(CheckBox)


    def GetResults(self):
        output = []
        for i in range(self.Layout.count()):
            LayoutWidget = self.Layout.itemAt(i).widget()
            if LayoutWidget.isChecked():
                output.append(LayoutWidget.text())

        return output
#}}}

#{{{ QQueryFilterInput: widget that abstracts the complexity of different datatypes and simplifies it to a single class
#it generates a SQL condition that later get added up together to get the full select query
#extreamly messy.
class QQueryFilterInput(QWidget):
    #the parameters are as follows
    #RecordData: a dictionary that contains the name of the record, its description, data type and optional values (like the possible values for enums and default values)
    def __init__(self, RecordData):
        super().__init__()
        self.Layout = QHBoxLayout(self)
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.Type = DataType(RecordData["Type"])
        self.RecordName = RecordData["Name"]
        self.Description = RecordData["Description"]

        self.EnableCheckBox = QCheckBox()
        self.EnableCheckBox.setFixedWidth(20)
        self.EnableCheckBox.stateChanged.connect(self.SetEnabledSilly)

        self.Label = QLabel(self.Description)
        self.Label.setMinimumSize(110, 25)
        self.Label.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        

        match self.Type:
            case DataType.BOOL: 
                self.InputWidget = QCheckBox()
                self.InputWidget.setFixedWidth(20)
            case DataType.ENUM:
                self.InputWidget = QListInputWidget(FileHelper.Config[RecordData["Values"]])
            case DataType.STRING:
                self.InputWidget = QLineEdit()
                if "DefaultValue" in RecordData:
                    self.InputWidget.setText(RecordData["DefaultValue"])
            case DataType.INT:
                self.InputWidget = QLineEdit("0")
                validator = QIntValidator(-10000, 100000)
                self.InputWidget.setValidator(validator)

                if "DefaultValue" in RecordData:
                    self.InputWidget.setText(str(RecordData["DefaultValue"]))
            case DataType.FLOAT:
                self.InputWidget = QLineEdit("0")
                validator = QDoubleValidator(-10000.0, 10000.0, 15)
                self.InputWidget.setValidator(validator)

                if "DefaultValue" in RecordData:
                    self.InputWidget.setText(str(RecordData["DefaultValue"]))
            case DataType.FAKEBOOL:
                self.InputWidget = QCheckBox()
                self.InputWidget.setFixedWidth(20)
            case _:
                FileHelper.ShowInfo("trying to make a MiscDataInputWidget with a datatype that isnt supported")

        self.Layout.addWidget(self.EnableCheckBox, Qt.AlignLeft)
        self.Layout.addWidget(self.Label, Qt.AlignLeft)

        #adding a widget to input operators, if its bool or fakebool then dont make the widget because bool values can only be true or false
        if self.Type != DataType.BOOL and self.Type != DataType.FAKEBOOL:
            self.OperatorInput = QComboBox()
            self.OperatorInput.addItems(DataTypeOperators[self.Type])
            self.OperatorInput.setFixedWidth(80)
            self.Layout.addWidget(self.OperatorInput, Qt.AlignLeft)
            self.Layout.setAlignment(self.OperatorInput, Qt.AlignLeft)

        self.Layout.addWidget(self.InputWidget, Qt.AlignRight)
        self.Layout.setAlignment(self.InputWidget, Qt.AlignRight)

        self.SetEnabled(False)

    def IsEnabled(self):
        return self.EnableCheckBox.isChecked()

    #this method exists for the sole reason that the stateChanged signal returns an enum instead of a normal bool value
    #so i use this function as a wrapper for the normal SetEnabled() function
    def SetEnabledSilly(self, NewValue):
        if NewValue == 2:
            self.SetEnabled(True)
        else:
            self.SetEnabled(False)

    def SetEnabled(self, NewValue):
        self.EnableCheckBox.setChecked(NewValue)
        self.InputWidget.setDisabled(not NewValue)
        if NewValue is True:
            self.InputWidget.show()
            if self.Type != DataType.BOOL and self.Type != DataType.FAKEBOOL:
                self.OperatorInput.show()
        else:
            self.InputWidget.hide()
            if self.Type != DataType.BOOL and self.Type != DataType.FAKEBOOL:
                self.OperatorInput.hide()

    def GetResults(self):
        if self.IsEnabled():
            if self.OperatorInput.currentText() == "CUSTOM":
                return "{} {}".format(self.RecordName, self.InputWidget.text())
            else:
                match self.Type:
                    case DataType.BOOL:
                        return "{} = {}".format(self.RecordName, self.InputWidget.isChecked())
                    case DataType.ENUM:
                        #so messy, but i cant bother to make it "nice"
                        return "{} {} (\"{}\")".format(self.RecordName, self.OperatorInput.currentText(), "\", \"".join(self.InputWidget.GetResults()))
                    case DataType.STRING:
                        return "{} {} \"{}\"".format(self.RecordName, self.OperatorInput.currentText(), self.InputWidget.text())
                    case DataType.INT | DataType.FLOAT:
                        return "{} {} {}".format(self.RecordName, self.OperatorInput.currentText(), self.InputWidget.text())
                    case DataType.FAKEBOOL:
                        if self.InputWidget.isChecked():
                            return self.RecordName + " >= 1"
                        else:
                            return self.RecordName + " = 0"

#}}}

#{{{ QFilterInputsContainer: a widget that contains filter inputs
class QFilterInputsContainer(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding)
        self.setFixedWidth(385)
        
        for i in RecordData.values():
            FilterWidget = QQueryFilterInput(i)
            self.Layout.addWidget(FilterWidget)

    def GetResults(self):
        #QuerySelection is a variable that will be Selection of the Query (aka filtering)
        QuerySelection = "where "
        FiltersEnabled = False

        for i in range(self.Layout.count()):
            LayoutWidget = self.Layout.itemAt(i).widget()
            if LayoutWidget.IsEnabled():
                #its kinda messy but
                #for the first time this code is run FiltersEnabled is ALWAYS false, and i dont want to add a "and" keyword in the first iteration
                #so i check whether FiltersEnabled is true and only then add the "and" keyword
                if FiltersEnabled:
                    QuerySelection += " and "
                QuerySelection += LayoutWidget.GetResults()
                FiltersEnabled = True

        #if there arent any filters enabled then dont return anything, because in SQL an empty "where" statment is a syntax error
        if FiltersEnabled:
            return QuerySelection
        else:
            return ""

    def RevertChanges(self):
        for i in range(self.Layout.count()):
            self.Layout.itemAt(i).widget().SetEnabled(False)
#}}}

#{{{ QOutputWidget: a widget for displaying planets
class QOutputWidget(QWidget):
    def __init__(self, Cords):
        super().__init__()
        self.Layout = QHBoxLayout(self)
        self.Cords = Cords

        self.DialogButton = QPushButton(self.Cords)
        self.DialogButton.setFlat(True)
        self.DialogButton.clicked.connect(self.ShowDialog)

        self.CopyButton = QPushButton("Copy")
        self.CopyButton.setFlat(True)
        self.CopyButton.setFixedWidth(75)
        self.CopyButton.clicked.connect(self.CopyCords)

        self.Layout.addWidget(self.DialogButton)
        self.Layout.addWidget(self.CopyButton)

    def ShowDialog(self):
        Dialog = QPlanetInfoDialog(self.Cords)
        Dialog.exec()

    def CopyCords(self):
        Clipboard = QApplication.clipboard()
        Clipboard.setText(self.Cords)
        FileHelper.ShowInfo("Coordinates copied!")
#}}}

#{{{ QOutputWidgetContainer: the widget containing the scroll area that has the output info
class QOutputWidgetContainer(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        self.setMinimumWidth(300)

        self.Label = QLabel("Output")

        self.OutputContent = QWidget()
        self.OutputLayout = QVBoxLayout(self.OutputContent)
        self.OutputLayout.setAlignment(Qt.AlignTop)


        self.OutputScrollArea = QScrollArea()
        self.OutputScrollArea.setWidgetResizable(True)
        self.OutputScrollArea.setWidget(self.OutputContent)

         

        self.Layout.addWidget(self.Label)
        self.Layout.addWidget(self.OutputScrollArea)


    def SetText(self, NewValue):

        #clearing every element from the layout
        i = self.OutputLayout.takeAt(0)
        while i:
            i.widget().deleteLater()
            i = self.OutputLayout.takeAt(0)

        for i, RowValue in enumerate(NewValue):
            Button = QOutputWidget(RowValue[0])

            if i >= MaxResults:
                break

            self.OutputLayout.addWidget(Button)

        self.OutputContent.show()
#}}}

#{{{ QInputWidgetContainer: the widget contaning color input widgets and the filter inputs
class QInputWidgetContainer(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        #i know this size looks like a hidden joke but its actually the perfect amount needed for the widget so stuff is placed correctly
        #if you make the value to like 400 then QQueryFilterInput for the string types will be a bit weirdo lookin
        self.setFixedWidth(420)


        self.MiscData = QFilterInputsContainer()
        self.MiscDataScrollArea = QScrollArea()
        self.MiscDataScrollArea.setWidget(self.MiscData)
        self.MiscDataScrollArea.setWidgetResizable(True)
        #self.MiscData.setFixedWidth(400)
        #self.MiscDataScrollArea.setMinimumHeight(1000)


        self.ApplyButton = QPushButton("Apply")
        self.ApplyButton.clicked.connect(self.Apply)

        self.ColorInputs = QColorInputContainer()

        self.Container = QWidget()
        self.ContainerLayout = QHBoxLayout(self.Container)

        self.RevertButton = QPushButton()
        self.RevertButton.clicked.connect(self.MiscData.RevertChanges)
        self.RevertButton.setFixedWidth(50)
        self.RevertButton.setFlat(True)
        self.RevertButton.setIcon(QIcon(str(FileHelper.RevertIconPath)))

        self.Label = QLabel("Filters")

        self.ContainerLayout.addWidget(self.Label)
        self.ContainerLayout.addWidget(self.RevertButton)

        self.Layout.addWidget(self.ApplyButton)
        self.Layout.addWidget(self.ColorInputs)
        self.Layout.addWidget(self.Container)
        self.Layout.addWidget(self.MiscDataScrollArea)


    def Apply(self):
        print("trying")
        try:
            QuerySelection = self.MiscData.GetResults()
            QueryProjection = "select {}, {}, {}, {}, {}, {}, {} from {} ".format(
                    RecordData["Cords"]["Name"], 
                    RecordData["PrimaryRed"]["Name"],
                    RecordData["PrimaryGreen"]["Name"],
                    RecordData["PrimaryBlue"]["Name"],
                    RecordData["SecondaryRed"]["Name"],
                    RecordData["SecondaryGreen"]["Name"],
                    RecordData["SecondaryBlue"]["Name"],
                    Table
                    )
            Query = DatabaseCursor.execute(QueryProjection + QuerySelection)

            if self.ColorInputs.IsEnabled():
                Output = RankQuery(Query, self.ColorInputs.GetMainColor(), self.ColorInputs.GetSecondaryColor())
            else:
                Output = Query.fetchall()

            self.parentWidget().OutputWidgets.SetText(Output)
            FileHelper.ShowInfo("Filters applied.")

        except Exception as error:
            FileHelper.ShowInfo(error)
#}}}

#{{{ MainWidget: a widget containing all other widgets
class MainWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__()
        self.Layout = QHBoxLayout(self)

        self.OutputWidgets = QOutputWidgetContainer()
        self.OutputWidgets.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)

        self.InputWidgets = QInputWidgetContainer()
        self.InputWidgets.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.Layout.addWidget(self.OutputWidgets)
        self.Layout.addWidget(self.InputWidgets)
#}}}
